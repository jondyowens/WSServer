module.exports.vertifyArguments = function(req, valid, optional){
    let errors = {};
    let fields = {};
    let success = true;

    if(valid){
        for(let i = 0; i < valid.length; i++){
            let key = valid[i];
            if(req.body[key] !== undefined)
                fields[key] = req.body[key];
            else if(req.query[key] !== undefined)
                fields[key] = req.query[key];
            else{
                errors[key] = "Required";
                success = false;
            }
        }
    }
    if (optional)
    {
        for (let i = 0; i < optional.length; i++)
        {
            let key = optional[i];
            if (req.body[key] !== undefined)
                fields[key] = req.body[key];
            else if (req.query[key] !== undefined)
                fields[key] = req.query[key];
        }
    }

    if (success)
        return { status: "success", data: fields };
    else
        return { status: "fail", reason: errors };
}

module.exports.sendError = function(message){
    return {
        status : 'fail',
        reason : message
    };
}

module.exports.sendSuccess = function(data){
    return {
        status : 'success',
        data : JSON.stringify(data)
    };
}