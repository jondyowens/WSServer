const MongoClient = require('mongodb').MongoClient;
const utils = require('./utils');
const assert = require('assert');

let url = 'mongodb://';
url = url.concat(process.env.DB_HOST);
url = url.concat(':');
url = url.concat(process.env.DB_PORT);

let dbInstance = null;
const dbName = process.env.DB_NAME;

module.exports.connect = function(){

    if(dbInstance){
        return process.nextTick(() => {
            callback(null, dbInstance);
        });
    } else {
        MongoClient.connect(url, function(err, client){
            console.log("Connected to MongoDB at " + url);
            dbInstance = client.db(dbName);
        })
    }
}

module.exports.insertDocument = function(document, callback){
    const collection = dbInstance.collection('users');

    
    collection.insertOne(document, (err, result) => {
        // there are multiple ways to catch errors. not sure an assert is useful for my purpose
        if(err){
            return process.nextTick(() => {callback(utils.sendError("Couldn't register new user."))})
        } else{
            return process.nextTick(() => { callback(utils.sendSuccess({username : document.username, password : document.password}))});
        }
    })
}

module.exports.getUser = function(username, callback){
    const collection = dbInstance.collection('users');
    let data = { username : username };
    collection.findOne(data, (err, result) => {
        assert.equal(err, null);
        if(result){
            return process.nextTick( () => { callback({username : result.username, hash : result.password, displayName : result.displayName }); } ) ;
            // we have a result
        } else {
            return process.nextTick( () => { callback(null)});
            // we don't 
        }
    })
}