require('dotenv').config();

const express = require('express');
const uuid = require('uuid-base62');
const users = require('./routes/route_user');
const app = express();
const mongo = require('./mongoFace');
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const apiRoot = '/api/v1/';
// connecting the webserver and listening for websocket connections
// from clients

app.get('/', function (req, res) {
    res.json({ "error": false, "message": "Hello !" });
});

users.register(app, apiRoot + 'users/');

app.listen(3000, function () {
    console.log('Example app listening at port 3000');
    mongo.connect();
});


// function onClientDisconnect(ws){
//     console.log("Client disconnected: " + ws);

//     let data = JSON.stringify({
//         msg : 'pdsc',
//         id : ws
//     });

//     delete SOCKET_LIST[ws];
//     wss.broadcast(data);
// }

// function onPlayerJoin(id){
//     console.log("received join message");
//     PLAYER_LIST.push(id);
// }

// function updateKeyPresses(){
//     //console.log("received keypress message");
// }
// // everything in here happens every 1/25 of a second
// function createPlayer(id){
//     for(let i = 0; i < frame.length; ++i){
//         if(frame[i].id == id){
//             return;
//         }
//     }
//     let player = {
//         id : id,
//         sprite : "/home/jondy/gitprojects/websockets/client/images/playerShip1_blue.png",
//         pos : {x : Math.random() * (800 - 0) + 0, y : 0},
//         v : {x : 0, y : 0},
//         rot : 0
//     }
//     frame.push(player);
// }
// function gameLoop() {
//     let data = JSON.stringify({
//         msg : 'fram',
//         frame : frame
//     })

//     wss.broadcast(data);
// }

// function sendWelcomeMessage(ws){
//         // composing message with their id
//         let message = JSON.stringify({
//             msg : "helo",
//             id : ws.id
//         });
//         // send message to all clients when client disconnects
//         ws.send(message);
// }
// // loops through all current players, if a player state is disconnected
// // it will delete them from the player list and broadcast to all others


// //game logic 25 times a second
// setInterval(function() {
//     gameLoop();
// }, 1000/25)

// wss.broadcast = function broadcast(data){
//     wss.clients.forEach(function each(client){
//         if(client.readyState === WebSocket.OPEN){
//             client.send(data);
//         }
//     })
// }


/* PLAYER CLASS EXAMPLE
    var Player = function(id){
    var self = {
        x:250,
        y:250,
        id:id,
        number:"" + Math.floor(10 * Math.random()),
        pressingRight:false,
        pressingLeft:false,
        pressingUp:false,
        pressingDown:false,
        maxSpd:10,
    }
    self.updatePosition = function(){
        if(self.pressingRight)
            self.x += self.maxSpd;
        if(self.pressingLeft)
            self.x -= self.maxSpd;
        if(self.pressingUp)
            self.y -= self.maxSpd;
        if(self.pressingDown)    // wss.on('connection', function connection(ws) {
    //     // assign them an id
    //     ws.id = uuid.v4();
    //     console.log(ws.id + " joined the server.");
    //     // add them to the socket list
    //     SOCKET_LIST[ws.id] = ws;
    //     sendWelcomeMessage(ws);


    //     ws.on('close', function(){
    //         onClientDisconnect(ws.id);
    //     });

    //     ws.on('message', function(msg){
    //         let message = JSON.parse(msg);
            
    //         switch(message.msg){
    //             case "join":
    //                 onPlayerJoin(message.id);
    //                 break;
    //             case "move":
    //                 updateKeyPresses();
    //                 break;
    //         }

    //     });
    // })
        spdX:0,
        spdY:0,
        id:"",
    }
    self.update = function(){
        self.updatePosition();
    }
    self.updatePosition = function(){
        self.x += self.spdX;
        self.y += self.spdY;
    }
    return self;
}
 
var Player = function(id){
    var self = Entity();
    self.id = id;
    self.number = "" + Math.floor(10 * Math.random());
    self.pressingRight = false;
    self.pressingLeft = false;
    self.pressingUp = false;
    self.pressingDown = false;
    self.maxSpd = 10;
   
    var super_update = self.update;
    self.update = function(){
        self.updateSpd();
        super_update();
    }
   
   
    self.updateSpd = function(){
        if(self.pressingRight)
            self.spdX = self.maxSpd;
        else if(self.pressingLeft)
            self.spdX = -self.maxSpd;
        else
            self.spdX = 0;
       
        if(self.pressingUp)
            self.spdY = -self.maxSpd;
        else if(self.pressingDown)
            self.spdY = self.maxSpd;
        else
            self.spdY = 0;     
    }
    Player.list[id] = self;
    return self;
}
Player.list = {};
Player.onConnect = function(socket){
    var player = Player(socket.id);
    socket.on('keyPress',function(data){
        if(data.inputId === 'left')
            player.pressingLeft = data.state;
        else if(data.inputId === 'right')
            player.pressingRight = data.state;
        else if(data.inputId === 'up')
            player.pressingUp = data.state;
        else if(data.inputId === 'down')
            player.pressingDown = data.state;
    });
}
Player.onDisconnect = function(socket){
    delete Player.list[socket.id];
}
Player.update = function(){
    var pack = [];
    for(var i in Player.list){
        var player = Player.list[i];
        player.update();
        pack.push({
            x:player.x,
            y:player.y,
            number:player.number
        });    
    }
    return pack;
}
 
 
var Bullet = function(angle){
    var self = Entity();
    self.id = Math.random();
    self.spdX = Math.cos(angle/180*Math.PI) * 10;
    self.spdY = Math.sin(angle/180*Math.PI) * 10;
   
    self.timer = 0;
    self.toRemove = false;
    var super_update = self.update;
    self.update = function(){
        if(self.timer++ > 100)
            self.toRemove = true;
        super_update();
    }
    Bullet.list[self.id] = self;
    return self;
}
Bullet.list = {};
 
Bullet.update = function(){
    if(Math.random() < 0.1){
        Bullet(Math.random()*360);
    }
   
    var pack = [];
    for(var i in Bullet.list){
        var bullet = Bullet.list[i];
        bullet.update();
        pack.push({
            x:bullet.x,
            y:bullet.y,
        });    
    }
    return pack;
}
*/
