const utils = require('./../utils');
const mongo = require('./../mongoFace');
// password encryption
const bcrypt = require('bcrypt');
const saltRounds = 10;
//..

function loginUser(req, res) {
    console.log("loginUser called");
    let args = utils.vertifyArguments(req, ["username", "password"]);
    if (args.status === 'fail') {
        console.log(args.reason);
        return;
    }

    // check database for username
    // verify if password is correct
    mongo.getUser(args.data.username.toLowerCase(), (result) => {
        if(result){
            bcrypt.compare(args.data.password, result.hash, (err, comp) => {
                if(comp === true){
                    return res.send(utils.sendSuccess({username : result.username, displayName : result.displayName}));
                    // password matches
                } else {
                    return res.send(utils.sendError("Username/password mismatch."));
                    // password mismatch
                }
            })
        } 
    })
}

function registerUser(req, res) {
    console.log("registerUser called");
    let args = utils.vertifyArguments(req, ["username", "password"]);
    if (args.status === 'fail') {
        console.log(args.reason);
        return;
    }

    // hashing password and sending to mongo
    let newUser = {};
    // TODO: how to properly use err and hash
    bcrypt.hash(args.data.password, saltRounds, (err, hash) => {
        newUser.username = args.data.username.toLowerCase();
        newUser.displayName = args.data.username;
        newUser.password = hash;

        mongo.insertDocument(newUser, (result) => {
            console.log(result);
            return res.send(result);
        });
    });

}

module.exports.register = function (app, root) {
    app.get(root + 'login', loginUser);
    app.post(root + 'register', registerUser);
}