# WebSockets Server

This is the server-side code for a websockets multiplayer game. It is inteded to be educational, as I'm using languages and tools that I wasn't directly taught in university. The server-side uses node.js and express for the server and MongoDB for the account database.

## Getting Started

The package.json has all the node package requirements. You will also need MongoDB running on your local machine.

### Prerequisites

What things you need to install the software and how to install them

```
MongoDB Server
NodeJS
Node modules (npm install)
Mocha (for unit tests)
Must have a .env file with the following defined:
DB_NAME=
DB_HOST=
DB_PORT=

SERVER_PORT=
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system


## Versioning

v0.0.0

## Authors

* **Jonathan Owens** - *Initial work* - [JondyOwens](https://gitlab.com/jondyowens)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

