const supertest = require('supertest');
const should = require('should');

let server = supertest.agent("http://localhost:3000");

describe("Testing User Register", function() {
  it("should return home page", function(done){
    server
    .get("/")
    .expect("Content-type", /json/)
    .expect(200)
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.error.should.equal(false);
      done();
    });
  });

  it("should create a new user", function(done){
    server
    .post('/api/v1/users/register')
    .send({username : "JONDY", password : "test" })
    .expect("Content-type", /json/)
    .expect(200)
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.status.should.equal('success');
      done();
    });
  });

  it("should fail because duplicate username", function(done){
    server
    .post('/api/v1/users/register')
    .send({username : "jOnDy", password : "test" })
    .expect("Content-type", /json/)
    .expect(200)
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.status.should.equal('fail');
      done();
    });
  });

  it("should successfully find user with correct password", function(done){
    server
    .get('/api/v1/users/login')
    .send({username: "JONdy", password : "test" })
    .expect("Content-type", /json/)
    .expect(200)
    .end(function(err, res){
      
      res.body.status.should.equal('success');
      res.body.data.should.equal(JSON.stringify({username : 'jondy', displayName : 'JONDY'}));
      done();
    });
  });
});